var expect    = require("chai").expect;

var map = require("../src/map.js");

describe("Scss dependency map", function() {

    beforeEach(function() {
        map.reset();
    });

    afterEach(function() {

    });

    it("should reference dependencies from file", function(){
        var a,
            b = ["a","c"],
            c = "f";

        map.add("b",b);
        expect(map.get("b")).to.deep.equal(b);
        map.add("c",c);
        expect(map.get("c")[0]).to.equal(c);
        map.add("a",a);
        expect(map.get("a")).to.be.empty;
    });

    it("should return path of all dependecies to file", function() {
        var a = ["c"],
            b = ["a", "c"],
            c = "f";

        map.add("a", a);
        map.add("b", b);
        map.add("c", c);

        expect(map.resolve("b")).to.be.empty
        expect(map.resolve("a")).to.deep.equal(["b"]);
        expect(map.resolve("c")).to.deep.equal(["a","b"]);
    })
});
