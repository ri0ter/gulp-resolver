var path = require('path');
var glob = require('glob');
var expect  = require("chai").expect;

var manager = require("../src/manager.js");

describe("Scss dependency manager", function() {

    var fixturesDir = path.join(__dirname, 'fixtures'),
        resolver;

    beforeEach(() => {
        resolver = manager({rootPath: 'fixtures'});
        //glob.sync(path.join(fixturesDir, '/**/*.scss'))
        //.map(function(file) {
        //    resolver(file);
        //    console.log(file);
        //});
    });

    afterEach(() => {

    });

    it("Should return empty array if file has no entry points", function() {
        return resolver(path.join(__dirname, 'fixtures/main.scss'))
        .then((entries) => {
            expect(entries).to.be.empty;
        })
    });

    it("Should return one entry point", function() {
        return resolver(path.join(__dirname, 'fixtures/main.scss'))
        .then(function() {
            return resolver(path.join(__dirname, 'fixtures/_one.parent.scss'))
        }).
        then(function(entries) {
            expect(entries).to.have.lengthOf(1);
        });
    });
});
