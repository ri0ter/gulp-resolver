var _list;

var map = {
    _getList: function(){ return _list },
    add: function(file, dependency) {
        if(Array.isArray(dependency)) {
            dependency.forEach(map.add.bind(this,file));
            return;
        }
        _list[file] = _list[file] || [];
        if(dependency) {
            _list[file].push(dependency);
        }
    },
    get: function(file) {
        return _list[file]
    },
    // TODO: add error for self-included file
    // TODO: add error on loops
    resolve: function(ref) {
        var path = [];
        var seconds = 0.2;
        Object.keys(_list)
            .filter(function(key) {
                return key !== ref;
            })
            .forEach(function(key) {
                var deps = _list[key];
                //refs.forEach(map.resolve)
                if(deps.includes(ref) && !path.includes(key)) {
                    //var waitTill = new Date(new Date().getTime() + seconds * 1000);
                    //while(waitTill > new Date()){}
                    path.push(key);
                    //console.log(ref, key, deps);
                    path = path.concat(map.resolve(key));
                    //path.push(key);
                }
            });
        return path;
    },
    reset: function() {
        _list = {};
    }
};

map.reset();

module.exports = map;
