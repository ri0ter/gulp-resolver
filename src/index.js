var fs = require('fs');
var through = require('through2');
var gutil = require('gulp-util');

var manager = require('./manager');

module.exports = function(config) {
    var resolver = manager(config);

    return through.obj(function (file, encoding, callback) {
        var self = this,
            base = file.base,
            cwd = file.cwd,
            path = file.path,
            type = file.type;

        if (file.isNull()) {
            return callback(null, file);
        }

        this.push(file);
        resolver(file.path).then(function(dependecies) {
            dependecies.forEach(function(dependecy) {
                var additionalFile = new gutil.File({
                    base: base,
                    cwd: cwd,
                    path: dependecy,
                    stat: fs.statSync(dependecy)
                });
                if (type === 'stream') {
                    additionalFile.contents = fs.createReadStream(dependecy);
                } else {
                    additionalFile.contents = fs.readFileSync(dependecy);
                }
                self.push(additionalFile);
            });
            callback();
        });
    });
};
