var map = require('./map');
var progeny = require('progeny');

var resolver = function(dependencyResolver) {
    return function(file) {
        var done = function(error, dependecies) {
            if(error) {
                throw error;
            }

            if(dependecies) {
                map.add(file, dependecies);
                //dependecies.forEach(dependencyResolver.bind(this, dependency, done))
                dependecies.forEach(function(dependency){
                    dependencyResolver(dependency, done);
                })
            }
            else {
                map.add(file);
            }
        };
        dependencyResolver(file, done);
    };
};

module.exports = function(config, callback) {
    return resolver(progeny(config))
};
