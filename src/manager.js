var progeny = require('progeny');

var map = require('./map');

var resolver = function(dependencyResolver) {
    return function resolve(file) {
/*        
        var done = function(error, dependecies) {
            if(error) {
                throw error;
            }
            console.log('dependecies', dependecies);
            if(dependecies) {
                map.add(file, dependecies);
                //dependecies.forEach(dependencyResolver.bind(this, dependency, done))
                dependecies.forEach(function(dependency){
                    dependencyResolver(dependency, done);
                })
            }
            else {
                map.add(file);
            }
        };
*/
        //return map.get(file);
        return new Promise(function(resolve, reject) {
            dependencyResolver(file, function(error, dependecies) {
                if(error) {
                    reject(error)
                }
                if(dependecies) {
                    map.add(file, dependecies);
                    /*dependecies.forEach(function(dependency){
                        dependencyResolver(dependency, done);
                    })*/
                }
                else {
                    map.add(file);
                }
                resolve(map.resolve(file));
            });

        });
    };
};

module.exports = function(config) {
    return resolver(progeny(config))
};

